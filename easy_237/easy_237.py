#!/usr/bin/python

import argparse

def opendictionary():
    try:
        dictionary = '/usr/share/dict/web2'
        dictionary = open(dictionary, 'r')
        dictionary = dictionary.read()
    except:
        print('Exception: Dictionary', dictionary, 'not found')
    return dictionary


def input(integers):
    lines = []
    for i in range(int(integers[0])):
        lines.append(raw_input('Please enter a string of chars:'))
    return lines

parser = argparse.ArgumentParser(description='Process some text')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                   help='an integer for the accumulator')
args = parser.parse_args()

dictionary=opendictionary()

lines=input(args.integers)
print(lines)

