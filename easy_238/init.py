#!/usr/bin/python

import argparse
import random

values = ['aeiou', 'bcdfghjklmnpqrstvwxyz']

def randomize(string):
    new_word = ''
    for i in string:
        if i == 'c':
            new_word = new_word + values[0][random.randint(0, (len(values[0])-1))]
        if i == 'v':
            new_word = new_word + values[1][random.randint(0, (len(values[1])-1))]
    return (new_word)


parser = argparse.ArgumentParser()
parser.add_argument('string', help="Enter a string of c's and v's")
args = parser.parse_args()

print(randomize(args.string))