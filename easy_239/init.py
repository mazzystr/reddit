#!/usr/bin/env python

'''
Reddit r/dailyprogrammer
https://www.reddit.com/r/dailyprogrammer/comments/3r7wxz/20151102_challenge_239_easy_a_game_of_threes/

Output time:
real	0m0.033s
user	0m0.020s
sys	0m0.011s
'''

import argparse


parser = argparse.ArgumentParser(description='Process some text')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                   help='an integer for the accumulator')
args = parser.parse_args()
number = args.integers[0]
iteration = 0
op = '0'
print('%s. %s %s' % (iteration, number, op))
while number > 1:
    if number%3:
        if number%3+1:
            number = number/3
            op = '+1'
        elif number%3-1:
            number = number/3
            op = '-1'
    else:
        number = number/3
        op = '0'
    iteration += 1
    print('%s. %s %s' % (iteration, number, op))
