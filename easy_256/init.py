#!/usr/bin/python

import argparse
import random
import string
import sys

def verify_size(a):
    return a if len(a) == 50 else None

def verify_content(a):
    for i, item in enumerate(a):
       if (i != 0 ) and (i != (len(a)-1)):
           if (a[i] == a[i-1]) and (a[i] == a[i+1]):
               a = None
               break
    return a

def verify_string(a):
     if a: a=verify_size(a)
     if a: a=verify_content(a)
     return a if a is a else None

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    a=''.join(random.choice(chars) for _ in range(size))
    return a

parser = argparse.ArgumentParser(description='Process some text')
parser.add_argument('--string', help='String help')
args = parser.parse_args()
if args.string:
    print verify_string(args.string)
else:
    a=id_generator(50, "6793YUIO")
    print verify_string(a)
