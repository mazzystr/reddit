```
$ go build go.go && ./go
Array of random numbers: [879117 642660 834965 349796 855298 178892 618327 450819 326947 929346]
Random Index is:  4
Winning number is:  855298
List of contestants: [{Gerald Lull 1} {Melissa Pearman Stambaugh 19} {Lisa Bailey 31} {Katie Scarlett 447} {Kimber Barrett 5092} {Tyler Winslowe 7450} {Teresa Chamberlain 7487} {Caleb Gillaspie 8700} {Jessica Collette 21420} {Webster Bergford 67845} {Elias Trinin 131313} {Sierra Myrick 147000} {Sam Cespedes 200000} {Lindsey Kupfer 317291} {Christopher Cope 317423} {Rebecca Rogers Whitehurst 424242} {Toni Bylsma 438000} {Dan Hinz 500001} {Eris Wheaton 525250} {Alex Caetano 654786} {Rachel James 777777}]
```

The winner in this example is Rachel James