package main

import (
  "bytes"
  "fmt"
  "io/ioutil"
  "os/exec"
  "regexp"
  "sort"
  "strconv"
  "strings"
)

type finalists struct {
    name string
    score int
}

func main() {
  var array [10]int
  var limit int = 1000000
  var randomIndex int

  winner := []finalists{}

  // Create an array of random numbers using openssl
  for i := 0; i < len(array); i++ {
    for {
      out, _ := exec.Command("/usr/bin/openssl", "rand", "-hex", "3").Output()
      outt, _ := strconv.ParseUint(
                      strings.TrimSuffix(
                          string(out[:]),
                          "\n"),
                      16,
                      64)
      if int(uint64(outt)) < limit {
        array[i] = int(uint64(outt))
        break
      }
    }
  }
  fmt.Printf("Array of random numbers: %v\n", array)

  // Use openssl to to find the first number under 10
  for {
    out, _ := exec.Command("/usr/bin/openssl", "rand", "-hex", "1").Output()
    outt, _ := strconv.ParseUint(
                    strings.TrimSuffix(
                        string(out[:]),
                        "\n"),
                    16,
                    64)
    if int(uint64(outt)) < len(array) {
      randomIndex = int(uint64(outt))
      break
    }
  }
  fmt.Println("Random Index is: ", randomIndex)

  // Display the WINNING number
  fmt.Println("Winning number is: ", array[randomIndex])

  // Import the data and make an attempt to sanitize
  data, _ := ioutil.ReadFile("/tmp/facebook")
  data = bytes.TrimPrefix(data, []byte(" "))
  data = bytes.ReplaceAll(data, []byte(","), []byte(""))
  data = bytes.ReplaceAll(data, []byte("^\n"), []byte(""))
  lines := string(data[:])
  re, _ := regexp.Compile("    Like\n")
  lines = re.ReplaceAllString(lines, "")
  re, _ = regexp.Compile("     · Reply ·.*\n")
  lines = re.ReplaceAllString(lines, "")
  re, _ = regexp.Compile("    .+\n")
  lines = re.ReplaceAllString(lines, "")
  re, _ = regexp.Compile("\\.[0-9]+\n")
  lines = re.ReplaceAllString(lines, "")
  re, _ = regexp.Compile("\\.+\n")
  lines = re.ReplaceAllString(lines, "")
  re, _ = regexp.Compile("\n\n")
  lines = re.ReplaceAllString(lines, "\n")
  liness := strings.Split(lines, "\n")

  // Iterate through the data
  for _, line := range liness {
    re, _ = regexp.Compile(" [0-9]+$")
    if re.MatchString(line) {
    entry := strings.Split(line, " ")
    if len(entry) >= 3 {
      outt, _ := strconv.ParseUint(
                      entry[len(entry)-1],
                      10,
                      64)
      if int(uint64(outt)) <= array[randomIndex] {
        winner = append(winner, finalists{name: strings.Join(entry[:len(entry)-1], " "), score: int(uint64(outt))})
      }
    }
    }
  }

  // Sort the slice
  sort.SliceStable(winner, func(i, j int) bool {
      return winner[i].score < winner[j].score
  })

  // Display list of contestants with the winner being the last element
  fmt.Println("List of contestants:", winner)
}