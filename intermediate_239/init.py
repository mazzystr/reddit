#!/usr/bin/env python

'''
Reddit r/dailyprogrammer
https://www.reddit.com/r/dailyprogrammer/comments/3rhzdj/20151104_challenge_239_intermediate_a_zerosum/
'''

import argparse
import random

def iterate(num):
    rand_value = 0
    op=0
    while num%3 != 0:
        rand_value = random.randint(-2, 2)
        num += rand_value
        if num%3 == 0:
            break
    num /= 3
    return num, rand_value


arg = []
funcs = []
funcs = ['plustwo', 'plusone', 'minusone', 'minustwo']

parser = argparse.ArgumentParser(description='Process some text')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                   help='an integer for the accumulator')
args = parser.parse_args()
arg.append(int(args.integers[0]))

print("Operation #\tInteger\t\tOperand")
while True:
    num = arg[0]
    iteration = 0
    ops = 0
    op = 0
    a = {}
    if iteration == 0:
        a[iteration] = [ arg[0] ]
    while num > 1:
        iteration += 1
        (num, op) = iterate(num)
        ops += op
        a[iteration] = [ num, op ]
    if (iteration != 0) and ( ops == 0):
        break
for k, v in a.iteritems():
    print(k),
    print("\t\t"),
    print(a[k][0]),
    print("\t\t"),
    print( ", ".join( repr(e) for e in a[k][1:] ) )
